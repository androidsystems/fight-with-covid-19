package com.sriidea.coronavirusupdate.network;

import com.sriidea.coronavirusupdate.CountryModel;
import com.sriidea.coronavirusupdate.ModelAll;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface API {

    @GET("countries")
    Call<List<CountryModel>> getAllCountryData();

    @GET("all")
    Call<ModelAll> getAllData();

}
