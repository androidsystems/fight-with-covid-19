package com.sriidea.coronavirusupdate;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import in.codeshuffle.typewriterview.TypeWriterView;

public class SplashActivity extends AppCompatActivity {

    private TypeWriterView typeWriterView;
    private Intent myIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Create Object and refer to layout view
        typeWriterView = findViewById(R.id.typeWriterView);

        //Setting each character animation delay
        typeWriterView.setDelay(4000);

        //Setting music effect On/Off
        typeWriterView.setWithMusic(true);

        //Animating Text
        typeWriterView.animateText("Staying Home, Staying Safe");

        myIntent = new Intent(this, MainActivity.class);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(myIntent);
                finish();
            }
        }, 4500);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}
