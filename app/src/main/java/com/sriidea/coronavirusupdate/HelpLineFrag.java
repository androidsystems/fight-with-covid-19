package com.sriidea.coronavirusupdate;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class HelpLineFrag extends Fragment {

    public HelpLineFrag() {
        // Required empty public constructor
    }

    private SearchView searchView;

    private ImageButton iv_refresh;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        searchView =
                ((MainActivity) Objects.requireNonNull(getActivity())).findViewById(R.id.sv_search_country);

        searchView.setVisibility(View.GONE);

        iv_refresh = ((MainActivity) Objects.requireNonNull(getActivity())).findViewById(R.id.iv_refresh);
        iv_refresh.setVisibility(View.GONE);

        return inflater.inflate(R.layout.fragment_help_line, container, false);
    }
}
