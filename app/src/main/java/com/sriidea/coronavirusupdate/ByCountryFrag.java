package com.sriidea.coronavirusupdate;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sriidea.coronavirusupdate.network.API;
import com.sriidea.coronavirusupdate.network.APIService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ByCountryFrag extends Fragment implements SearchView.OnQueryTextListener {

    private static final String TAG = "sri" + ByCountryFrag.class.getSimpleName();

    private RecyclerView recyclerView;
    private List<CountryModel> countryList;
    private ByCountryAdapter mAdapter;

    private SearchView searchView;
    private View view;

    private ProgressBar progress;

    private ImageButton iv_refresh;

    public static ByCountryFrag newInstance(String param1, String param2) {
        ByCountryFrag byCountryFrag = new ByCountryFrag();
        Bundle bundle = new Bundle();
        byCountryFrag.setArguments(bundle);
        return byCountryFrag;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_by_country, container, false);

        progress = view.findViewById(R.id.progress);

        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        recyclerView = view.findViewById(R.id.recycler_view);
        countryList = new ArrayList<>();
        mAdapter = new ByCountryAdapter(getActivity(), countryList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(8), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        getAllCountryData();

        iv_refresh = ((MainActivity) Objects.requireNonNull(getActivity())).findViewById(R.id.iv_refresh);
        iv_refresh.setVisibility(View.VISIBLE);
        iv_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllCountryData();
            }
        });

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getSearchProduct() {
        searchView =
                ((MainActivity) Objects.requireNonNull(getActivity())).findViewById(R.id.sv_search_country);

        searchView.setVisibility(View.VISIBLE);
        searchView.setOnQueryTextListener(this);
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getAllCountryData() {

        // checking internet
        if (!APIService.checkInter(Objects.requireNonNull(getContext()))) {
            Toast.makeText(getContext(), "You Have no internet", Toast.LENGTH_LONG).show();
        }

        ((TextView) view.findViewById(R.id.tv_offline))
                .setVisibility(View.GONE);

        ((ProgressBar) view.findViewById(R.id.progress))
                .setVisibility(View.VISIBLE);

        API api = APIService.getClinet().create(API.class);
        Call<List<CountryModel>> call = api.getAllCountryData();

        call.enqueue(new Callback<List<CountryModel>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<List<CountryModel>> call, Response<List<CountryModel>> response) {

                progress.setVisibility(View.GONE);

                try {

                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        Log.d(TAG, "response: " + response.body().size());

                        List<CountryModel> countryModelList = response.body();
                        countryList.clear();
                        countryList.addAll(countryModelList);
                        mAdapter.notifyDataSetChanged();

                        getSearchProduct();
                    } else {
                        getOffLineData(loadJsonFromAsset());
                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();
                    getOffLineData(loadJsonFromAsset());
                }

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onFailure(Call<List<CountryModel>> call, Throwable t) {
                progress.setVisibility(View.GONE);

                getOffLineData(loadJsonFromAsset());
            }
        });

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        newText = newText.toLowerCase();
        ArrayList<CountryModel> newListProductData = new ArrayList<>();
        for (CountryModel data : countryList) {
            String getName = data.getCountry().toLowerCase();
            if (getName.contains(newText)) {
                newListProductData.add(data);
            }
        }
        mAdapter.filters(newListProductData);
        return false;
    }


    private class ByCountryAdapter extends RecyclerView.Adapter<ByCountryAdapter.MyViewHolder> {
        private Context context;
        private List<CountryModel> movieList;

        public void filters(ArrayList<CountryModel> countryModels) {

            movieList = new ArrayList<>();
            movieList.addAll(countryModels);
            notifyDataSetChanged();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView name, tv_cases_value, tv_death_value, tv_recover_value;
            private CardView card_view;

            public MyViewHolder(View view) {
                super(view);
                name = view.findViewById(R.id.tvCountryName);
                tv_cases_value = view.findViewById(R.id.tv_cases_value);
                tv_death_value = view.findViewById(R.id.tv_death_value);
                tv_recover_value = view.findViewById(R.id.tv_recover_value);
                card_view = view.findViewById(R.id.card_view);
            }
        }


        public ByCountryAdapter(Context context, List<CountryModel> movieList) {
            this.context = context;
            this.movieList = movieList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.single_country_row, parent, false);

            return new MyViewHolder(itemView);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final CountryModel movie = movieList.get(position);

            holder.name.setText("" + movie.getCountry());

            APIService.animateTextView(0, movie.getCases(), holder.tv_cases_value);
            APIService.animateTextView(0, movie.getDeaths(), holder.tv_death_value);
            APIService.animateTextView(0, movie.getRecovered(), holder.tv_recover_value);


            holder.card_view.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.CUPCAKE)
                @Override
                public void onClick(View v) {

                    Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = inflater.inflate(R.layout.single_country_dialog, null, false);

                    ((TextView) view.findViewById(R.id.tvCountryName)).setText("" + movie.getCountry());

                    APIService.animateTextView(0, movie.getCases(),
                            ((TextView) view.findViewById(R.id.tv_cases_value)));

                    APIService.animateTextView(0, movie.getTodayCases(),
                            ((TextView) view.findViewById(R.id.tv_cases_value_new)));

                    APIService.animateTextView(0, movie.getDeaths(),
                            ((TextView) view.findViewById(R.id.tv_death_value)));

                    APIService.animateTextView(0, movie.getTodayDeaths(),
                            ((TextView) view.findViewById(R.id.tv_death_value_new)));

                    APIService.animateTextView(0, movie.getRecovered(),
                            ((TextView) view.findViewById(R.id.tv_recover_value)));

                    APIService.animateTextView(0, movie.getActive(),
                            ((TextView) view.findViewById(R.id.tv_active_value)));

                    APIService.animateTextView(0, movie.getCritical(),
                            ((TextView) view.findViewById(R.id.tv_critical_value)));

                    APIService.animateTextView(0, movie.getCasesPerOneMillion(),
                            ((TextView) view.findViewById(R.id.tv_cpm_value)));

                    APIService.animateTextView(0, movie.getDeathsPerOneMillion(),
                            ((TextView) view.findViewById(R.id.tv_dpm_value)));

                    ((TextView) view.findViewById(R.id.tv_init_case_value)).setText("" + movie.getFirstCase());

                    ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                    dialog.setContentView(view);
                    final Window window = dialog.getWindow();
                    window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                    window.setBackgroundDrawableResource(android.R.color.white);
                    window.setGravity(Gravity.CENTER);
                    dialog.show();

                }

            });

        }

        @Override
        public int getItemCount() {
            return movieList.size();
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getOffLineData(String loadJsonFromAsset) {
        try {

            JSONArray jsonArray = new JSONArray(loadJsonFromAsset);

            Log.d("json", "data: " + jsonArray.length());

            List<CountryModel> modelList = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String country = jsonObject.getString("country");
                String cases = jsonObject.getString("cases");
                String todayCases = jsonObject.getString("todayCases");
                String deaths = jsonObject.getString("deaths");
                String todayDeaths = jsonObject.getString("todayDeaths");
                String recovered = jsonObject.getString("recovered");
                String active = jsonObject.getString("active");
                String critical = jsonObject.getString("critical");
                String casesPerOneMillion = jsonObject.getString("casesPerOneMillion");
                String deathsPerOneMillion = jsonObject.getString("deathsPerOneMillion");
                String firstCase = jsonObject.getString("firstCase");

                CountryModel model = new CountryModel
                        (
                                country,
                                Integer.parseInt(cases),
                                Integer.parseInt(todayCases),
                                Integer.parseInt(deaths),
                                Integer.parseInt(todayDeaths),
                                Integer.parseInt(recovered),
                                Integer.parseInt(active),
                                Integer.parseInt(critical),
                                Integer.parseInt(casesPerOneMillion),
                                Integer.parseInt(deathsPerOneMillion),
                                firstCase
                        );
                modelList.add(model);
            }
            countryList.clear();
            countryList.addAll(modelList);
            mAdapter.notifyDataSetChanged();
            getSearchProduct();

            ((TextView) view.findViewById(R.id.tv_offline))
                    .setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.tv_offline))
                    .setText(R.string.offline_update_note);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String loadJsonFromAsset() {

        String json = null;

        try {

            InputStream is = getActivity().getAssets().open("json/all_data_country");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

}
