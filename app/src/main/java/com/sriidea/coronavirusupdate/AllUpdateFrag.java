package com.sriidea.coronavirusupdate;

import android.animation.ValueAnimator;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sriidea.coronavirusupdate.network.API;
import com.sriidea.coronavirusupdate.network.APIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class AllUpdateFrag extends Fragment {

    private View view;
    private ImageButton iv_refresh;
    private TextView tv_cases_value, tv_death_value, tv_recover_value;

    public AllUpdateFrag() {
        // Required empty public constructor
    }

    private ProgressBar progressBar;
    private SearchView searchView;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_all_update, container, false);
        init();

        searchView =
                ((MainActivity) Objects.requireNonNull(getActivity())).findViewById(R.id.sv_search_country);

        searchView.setVisibility(View.GONE);

        tv_cases_value = view.findViewById(R.id.tv_cases_value);
        tv_death_value = view.findViewById(R.id.tv_death_value);
        tv_recover_value = view.findViewById(R.id.tv_recover_value);

        return view;
    }

    private void init() {
        getAllData();

        progressBar = view.findViewById(R.id.progress);

        iv_refresh = ((MainActivity) Objects.requireNonNull(getActivity())).findViewById(R.id.iv_refresh);
        iv_refresh.setVisibility(View.VISIBLE);
        iv_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllData();
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getAllData() {
        ((TextView) view.findViewById(R.id.tv_offline))
                .setVisibility(View.GONE);
        ((ProgressBar) view.findViewById(R.id.progress))
                .setVisibility(View.VISIBLE);


        API api = APIService.getClinet().create(API.class);
        Call<ModelAll> modelAllCall = api.getAllData();

        modelAllCall.enqueue(new Callback<ModelAll>() {
            @Override
            public void onResponse(Call<ModelAll> call, Response<ModelAll> response) {

                progressBar.setVisibility(View.GONE);

                try {
                    if (response.isSuccessful()) {

                        assert response.body() != null;
                        APIService.animateTextView(0, response.body().getCases(), tv_cases_value);
                        APIService.animateTextView(0, response.body().getDeaths(), tv_death_value);
                        APIService.animateTextView(0, response.body().getRecovered(), tv_recover_value);

                    } else {
                        getOffLineData(loadJsonFromAsset());
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    Log.d("error", "error: " + e.toString());
                    getOffLineData(loadJsonFromAsset());
                }


            }

            @Override
            public void onFailure(Call<ModelAll> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                getOffLineData(loadJsonFromAsset());
                Log.d("error", "error on Fail: " + t.toString());
            }
        });
    }

    private void getOffLineData(String loadJsonFromAsset) {
        try {

            JSONObject jsonObject = new JSONObject(loadJsonFromAsset);

            String cases = jsonObject.getString("cases");
            String deaths = jsonObject.getString("deaths");
            String recovered = jsonObject.getString("recovered");

            APIService.animateTextView(0, Integer.parseInt(cases), tv_cases_value);
            APIService.animateTextView(0, Integer.parseInt(deaths), tv_death_value);
            APIService.animateTextView(0, Integer.parseInt(recovered), tv_recover_value);

            ((TextView) view.findViewById(R.id.tv_offline))
                    .setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.tv_offline))
                    .setText(R.string.offline_update_note);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String loadJsonFromAsset() {

        String json = null;

        try {

            InputStream is = getActivity().getAssets().open("json/all_data");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }


}
